# TrackPad

This is a *very* simple server/client app in python to allow a device with [Pythonista](http://omz-software.com/pythonista) to serve as a trackpad to a computer running the server application

### Usage

Run `python server.py` on the computer you want to control. It will tell you at which port it is listening.

Run `client.py` on Pythonista and enter the IP address of your machine and which port it is listening in.
