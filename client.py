import console
from collections import namedtuple
import objc_util
import socket
import struct
import time
import ui

SENSIBILITY = 1       # 3D force that triggers a mouse down event
THROTTLE_GAP = 0.01   # only send events after this amount of time
SERVER = (None, None) # server ip, leave as None to enter it via input

class Touches:
  def __init__(self):
    self.touches = {}
    self.Touch = namedtuple('Touch', 'id location')
    self.main_touch = self.Touch(None, None)

  def click_state(self):
    def is_left_click():
      main_id = self.main_touch.id
      if main_id in self.touches:
        touch = self.touches[main_id]
        ui_touch = objc_util.ObjCInstance(touch)
        if touch.phase in ['ended', 'cancelled']:
          self.handle_touch(touch)
          return False
        tap_count = ui_touch.tapCount()
        force = ui_touch.force()
        if touch.phase == 'ended':
          return False
        return force >= SENSIBILITY or tap_count >= 2
      return False
    def is_right_click():
      return len(self.touches) == 2
    return (is_left_click(), is_right_click())

  def handle_touch(self, touch):
    id = touch.touch_id
    if touch.phase in ['began', 'moved']:
      self.touches[id] = touch
    elif id in self.touches:
      del self.touches[id]
    self.update_main_touch(location = False)

  def update_main_touch(self, location = True):
    if self.main_touch.id == None:
      touches = self.touches.values()
      if len(self.touches):
        touch = list(touches)[0]
        self.main_touch = self.Touch(touch.touch_id, touch.location)
    else:
      id = self.main_touch.id
      if id in self.touches:
        if location:
          touch = self.touches[id]
          self.main_touch = self.Touch(touch.touch_id, touch.location)
      else:
        self.main_touch = self.Touch(None, None)
        self.update_main_touch()

class TrackPad (ui.View):
  def __init__(self):
    self.bg_color = 'white'
    self.last_location = None
    self.last_event = time.time()
    self.touches = Touches()
    self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    ip = SERVER[0] or console.input_alert("Server Address")
    port = SERVER[1] or int(console.input_alert("Server Port"))
    self.socket.connect((ip, port))

  def send_event(self, touch):
    location = (0, 0)
    if touch.touch_id == self.touches.main_touch.id:
      location = touch.location - self.touches.main_touch.location
    w, h = ui.get_screen_size()
    x, y = location
    x = x / w
    y = y / h
    left, right = self.touches.click_state()
    self.socket.send(struct.pack('!f f ? ?', x, y, left, right))
    self.touches.update_main_touch()

  def handle_event(self, touch):
    if touch.phase == 'moved':
      now = time.time()
      if now - self.last_event > THROTTLE_GAP:
        self.touches.handle_touch(touch)
        self.send_event(touch)
        self.last_event = now
    else:
      self.touches.handle_touch(touch)
      self.send_event(touch)

  def touch_began(self, touch):
    self.handle_event(touch)

  def touch_moved(self, touch):
    self.handle_event(touch)

  def touch_ended(self, touch):
    self.handle_event(touch)

pad = TrackPad()
pad.name = 'TrackPad'
pad.present('fullscreen')
