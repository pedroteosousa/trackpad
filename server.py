from collections import namedtuple
import mouse
import pyautogui
import random
import socket
import struct
import subprocess
import time

SCREEN_SIZE = pyautogui.size()
STRUCT_STR = '!f f ? ?'
STRUCT_BYTES = 4 + 4 + 1 + 1

MouseState = namedtuple('MouseState', 'left right')
mouse_state = MouseState(False, False)

def update_state(prev_state, next_state):
    def update_button(prev_pressed, next_pressed, button_id):
        if next_pressed and not prev_pressed:
            subprocess.call(["xdotool", "mousedown", button_id])
        if not next_pressed and prev_pressed:
            subprocess.call(["xdotool", "mouseup", button_id])
    update_button(prev_state.left, next_state.left, "1")
    update_button(prev_state.right, next_state.right, "3")
    return next_state

def bind_in_range(server, low, high):
    retries = 5
    while True:
        try:
            port = random.randint(low, high)
            server.bind(('', port))
            return port
        except:
            retries -= 1
            if retries == 0:
                print('could not bind socket')
                exit(1)
            pass


server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
port = bind_in_range(server, 3000, 3200)
server.listen(1)

print('listening at port', port)

while True:
    (client, address) = server.accept()
    print('established connection:', address)
    while True:
        data = b''
        try:
            while len(data) < STRUCT_BYTES:
                received = client.recv(STRUCT_BYTES - len(data))
                if not received:
                    raise socket.error('client not responding')
                data += received
        except socket.error as err:
            print(err)
            print('closing connection...')
            client.close()
            break
        x, y, left, right = struct.unpack(STRUCT_STR, data)
        x = round(x * SCREEN_SIZE[0])
        y = round(y * SCREEN_SIZE[1])
        mouse.move(x, y, absolute = False)
        mouse_state = update_state(mouse_state, MouseState(left, right))
